package core;

import org.jetbrains.annotations.Nullable;

public final class PointNotReachableException extends Exception {
    PointNotReachableException(@Nullable final String s) {
        super(s);
    }
}
