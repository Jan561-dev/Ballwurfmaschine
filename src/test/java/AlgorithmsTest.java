import core.Algorithms;
import core.PointNotReachableException;
import org.jetbrains.annotations.Contract;
import org.junit.Test;

public final class AlgorithmsTest {

    //PUBLIC

    @Test
    public void testAngleCalculationVerticalReachable() throws PointNotReachableException {
        final double calculated = Algorithms.calcAngleFrictionAlgorithm(0, 5, 0, 15, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value, 8, null);
        assert(doubleEquals(calculated, 90));
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Test(expected = PointNotReachableException.class)
    public void testAngleCalculationVerticalNotReachable() throws PointNotReachableException {
        Algorithms.calcAngleFrictionAlgorithm(0, 5, 0, 10, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value, 8, null);
    }

    @Test
    public void testAngleCalculationNotVerticalReachable() throws PointNotReachableException {
        final double calculated = Algorithms.calcAngleFrictionAlgorithm(11.41, 3.13, 1.5, 15, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value, 8, null);
        assert(doubleEquals(calculated, 39)) : calculated;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Test(expected = PointNotReachableException.class)
    public void testAngleCalculationNotVerticalNotReachable() throws PointNotReachableException {
        Algorithms.calcAngleFrictionAlgorithm(12, 3, 1.5, 15, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value, 8, null);
    }

    //PRIVATE

    @Contract(pure = true)
    private static boolean doubleEquals(final double d1, final double d2) {
        return Math.abs(d1-d2) < 0.1;
    }

}
