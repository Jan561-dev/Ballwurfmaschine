import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gui.MainController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{

    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
        Parent parent = loader.load();

        primaryStage.setScene(new Scene(parent, 500, 300));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Angle Calculation");
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> Platform.exit());
    }
}
